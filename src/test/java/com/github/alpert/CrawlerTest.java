package com.github.alpert;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.testng.reporters.Files;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Jsoup.class)
public class CrawlerTest {
    static final Document emptyDocument = Jsoup.parse("");

    @Test
    public void s() throws IOException {
        final String keyword = "java";
        final String encodedKeyword = URLEncoder.encode(keyword, StandardCharsets.UTF_8.name());
        final String javaUrl = "https://www.java.com/de/download/";

        final InputStream dummyGoogle =
            Thread.currentThread().getContextClassLoader().getResourceAsStream("dummy_google.html");
        assert dummyGoogle != null;
        final Document googleDoc = Jsoup.parse(Files.readFile(dummyGoogle));
        dummyGoogle.close();

        final InputStream dummyJava =
            Thread.currentThread().getContextClassLoader().getResourceAsStream("dummy_java.html");
        assert dummyJava != null;
        final Document javaDoc = Jsoup.parse(Files.readFile(dummyJava));
        dummyJava.close();

        final Connection googleConnection = mock(Connection.class);
        mockStatic(Jsoup.class);
        when(Jsoup.connect(Crawler.GOOGLE + encodedKeyword)).thenReturn(googleConnection);
        when(googleConnection.userAgent(anyString())).thenReturn(googleConnection);
        when(googleConnection.get()).thenReturn(googleDoc);

        final Connection javaConnection = mock(Connection.class);
        when(Jsoup.connect(javaUrl)).thenReturn(javaConnection);
        when(javaConnection.userAgent(anyString())).thenReturn(javaConnection);
        when(javaConnection.get()).thenReturn(javaDoc);

        final Map<String, Long> top5JSLib = Crawler.crawl("java");

        assertTrue(top5JSLib.containsKey("apmeum"));
        assertTrue(top5JSLib.containsKey("popUp"));
        assertTrue(top5JSLib.containsKey("jquery"));

        assertEquals(3, top5JSLib.get("popUp").longValue());
        assertEquals(2, top5JSLib.get("jquery").longValue());
        assertEquals(1, top5JSLib.get("apmeum").longValue());
    }

    @Test
    public void ss() throws IOException {
        final String keyword = "java";
        final String encodedKeyword = URLEncoder.encode(keyword, StandardCharsets.UTF_8.name());

        final Connection googleConnection = mock(Connection.class);
        mockStatic(Jsoup.class);
        when(Jsoup.connect(Crawler.GOOGLE + encodedKeyword)).thenReturn(googleConnection);
        when(googleConnection.userAgent(anyString())).thenReturn(googleConnection);
        when(googleConnection.get()).thenThrow(new RuntimeException("Timeout"));

        final Map<String, Long> top5JSLib = Crawler.crawl("java");
        assertTrue(top5JSLib.isEmpty());
    }

    @Test
    public void shouldExtractLinksFromValidGoogleSearch() throws IOException {
        final List<String> expectedLinks = Arrays.asList("https://www.java.com/de/download/",
            "https://www.java.com/de/",
            "https://de.wikipedia.org/wiki/Java",
            "https://de.wikipedia.org/wiki/Java-Technologie",
            "https://www.chip.de/downloads/Java-Runtime-Environment-32-Bit_13014576.html",
            "https://www.awacademy.de/program/java-academy-open-fruhling-2020",
            "https://wiki.zum.de/wiki/Java");
        final InputStream resourceAsStream =
            Thread.currentThread().getContextClassLoader().getResourceAsStream("google.html");
        assert resourceAsStream != null;
        final Document document = Jsoup.parse(Files.readFile(resourceAsStream));

        final List<String> actualLinks = Crawler.extractLinks(document)
            .collect(Collectors.toList());

        assertThat(actualLinks, is(expectedLinks));
    }

    @Test
    public void shouldReturnEmptyIfNoLinks() {
        final Stream<String> emptyStream = Crawler.extractLinks(emptyDocument);
        assertEquals(0, emptyStream.count());
    }

    @Test
    public void shouldExtractJSLibrariesFromValidHtml() throws IOException {
        final List<String> expectedJSLibraries = Arrays.asList("apmeum", "popUp");
        final InputStream resourceAsStream =
            Thread.currentThread().getContextClassLoader().getResourceAsStream("java.html");
        assert resourceAsStream != null;
        final Document document = Jsoup.parse(Files.readFile(resourceAsStream));

        final List<String> actualJSLibraries = Crawler.extractJSLibraries(document)
            .collect(Collectors.toList());

        assertThat(actualJSLibraries, is(expectedJSLibraries));
    }

    @Test
    public void shouldReturnEmptyIfNoJS() {
        final Stream<String> emptyStream = Crawler.extractJSLibraries(emptyDocument);
        assertEquals(0, emptyStream.count());
    }

    @Test
    public void pathValidityTest() {
        assertTrue(Crawler.isValidSource(
            "https://omc-7e2e90dc30bda.eum.omc.ocp.oraclecloud"
                + ".com/APMaaSCollector/external/collector/staticlib/apmeum.js"));
        assertTrue(Crawler.isValidSource("/ga/js/popUp.js"));
        assertTrue(Crawler
            .isValidSource("https://149351115.v2.pressablecdn.com/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp"));


        assertFalse(Crawler.isValidSource(""));
        assertFalse(Crawler.isValidSource("https://dummy.com/script.php&ga.js/popUp.js"));
        assertFalse(Crawler.isValidSource(null));
    }
}
