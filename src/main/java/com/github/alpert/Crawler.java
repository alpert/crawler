package com.github.alpert;

import org.jsoup.Jsoup;
import org.jsoup.internal.StringUtil;
import org.jsoup.nodes.Document;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

public class Crawler {
    static final String GOOGLE = "http://www.google.com/search?q=";
    // Using custom agent because the result page of Google changes based on agent and
    // that version is the easiest to parse
    static final String USER_AGENT =
        "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 "
            + "Safari/537.36";

    public static void main(String[] args) {
        if (args.length < 1 || StringUtil.isBlank(args[0])) {
            System.out.println(usage());
            System.exit(0);
        }

        final Map<String, Long> top5JSLibrary = crawl(args[0]);

        top5JSLibrary.forEach((k, v) ->
            System.out.println("Library: " + k + " is found " + v + " times."));
    }

    /**
     * Crawler uses parallel streams to achieve better performance.
     */
    static Map<String, Long> crawl(final String searchPhrase) {
        final String encodedSearchPhrase;
        try {
            encodedSearchPhrase = URLEncoder.encode(searchPhrase, StandardCharsets.UTF_8.name());
        } catch (UnsupportedEncodingException e) {
            // as we use a standard charset that should never happen
            return new HashMap<>();
        }

        final Stream<String> links = retrieveHtml(GOOGLE + encodedSearchPhrase)
            .map(Crawler::extractLinks)
            .orElse(Stream.empty());

        final Map<String, Long> allJSLibraries = links
            .map(Crawler::retrieveHtml)
            .filter(Optional::isPresent)
            .flatMap(document -> extractJSLibraries(document.get()))
            .collect(groupingBy(Function.identity(), counting()));

        return allJSLibraries
            .entrySet().stream()
            .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
            .limit(5)
            .collect(Collectors.toMap(
                Map.Entry::getKey,
                Map.Entry::getValue,
                (v1, v2) -> {
                    throw new IllegalStateException();
                }, LinkedHashMap::new
            ));
    }

    /**
     * The main links in Google result page is inside a div element as follows:
     *
     * <div class="r">
     *     <a href="https://www.java.com/de/download/"
     *        ping="/url?sa=t&amp;source=web&amp;rct=j&amp;url=https://www.java.com/de/download/&amp;
     *        ved=2ahUKEwjdqsrx7aDmAhWIZVAKHUptDFUQFjAAegQIEBAC">
     *         <h3 class="LC20lb"><span class="S3Uucc">Download der kostenlosen Java-Software</span></h3><br>
     *         <div class="TbwUpd">
     *             <cite class="iUh30 bc">https://www.java.com › download</cite>
     *         </div>
     *     </a>
     * </div>
     */
    static Stream<String> extractLinks(final Document document) {
        return document
            .select("div.r")
            .parallelStream()
            .map(c -> c.selectFirst("a[href]").absUrl("href"));
    }

    /**
     * We try to parse `src` attribute of `script` elements. Then we try to extract name of the script file.
     * For example if script element is: <script type="text/javascript" src="/ga/js/popUp.js"></script> then
     * we return just `popUp`. That way we consider following as same:
     *
     * * <script type="text/javascript" src="https:.../jquery.js"></script>
     * * <script type="text/javascript" src="https:.../jquery.js?ver=1.12.4-wp"></script>
     * * <script type="text/javascript" src="https:.../jquery.min.js"></script>
     */
    static Stream<String> extractJSLibraries(final Document document) {
        return document
            .select("script")
            .parallelStream()
            .map(element -> element.attr("src"))
            .filter(Crawler::isValidSource)
            .map(l -> l.substring(l.lastIndexOf("/") + 1, l.indexOf(".js") + 3))
            .map(l -> l.substring(0, l.indexOf(".")));
    }

    /**
     * We consider following script sources as invalid:
     *
     * * null
     * * empty string
     * * does not contains `.js`
     * * contains `/` after `.js`
     */
    static boolean isValidSource(final String scriptSource) {
        return !StringUtil.isBlank(scriptSource)
            && scriptSource.contains(".js")
            && scriptSource.lastIndexOf("/") + 1 < scriptSource.indexOf(".js") + 3;
    }

    static Optional<Document> retrieveHtml(final String url) {
        try {
            return Optional.of(Jsoup.connect(url).userAgent(USER_AGENT).get());
        } catch (Exception e) {
            System.err
                .println("Error: Could not connect and retrieve " + url + " Exception message: " + e.getMessage());
            return Optional.empty();
        }
    }

    static String usage() {
        return "Usage: ./crawler word\n"
            + "\n"
            + "You can use double quotes to search a phrase. Eg: ./crawler \"viva la vida\"";
    }
}
